Feature: Hacer busqueda en Google

  Background: Se ingresa a Google
    Given Estoy en la pagina principal de Google

  @busqueda @google @1.1 @regresion
  Scenario: Buscar una palabra o frase en google y corroborar resultados
    When Busco una palabra o frase
    Then Se despliegan resultados relacionados

  @busqueda @google @imagen @1.2
  Scenario: Buscar una palabra o frase en google y corroborar imagenes
    When Busco una palabra o frase
    And Ingreso a la seccion de imagenes
    Then Se despliegan imagenes relacionadas con la busqueda
