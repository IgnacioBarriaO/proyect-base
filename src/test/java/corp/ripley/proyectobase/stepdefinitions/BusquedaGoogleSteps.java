package corp.ripley.proyectobase.stepdefinitions;

import corp.ripley.proyectobase.models.PalabraOFrase;
import corp.ripley.proyectobase.models.World;
import corp.ripley.proyectobase.pageobjects.GoogleHomePage;
import corp.ripley.proyectobase.pageobjects.GoogleSearchPage;
import corp.ripley.proyectobase.utils.MetodosGenericos;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.io.IOException;
import java.util.Properties;

public class BusquedaGoogleSteps {

    GoogleHomePage googleHomePage;
    GoogleSearchPage googleSearchPage;
    World world;
    //Se le pone como arugmento la ruta donde estarán los properties
    Properties prop = MetodosGenericos.determinarPropertiesPorAmbiente("src/test/resources/properties");

    //Constructor
    public BusquedaGoogleSteps(World world, GoogleHomePage googleHomePage, GoogleSearchPage googleSearchPage) throws IOException {
        this.world = world;
        this.googleHomePage = googleHomePage;
        this.googleSearchPage = googleSearchPage;
    }

    @Given("Estoy en la pagina principal de Google")
    public void estoyEnLaPaginaPrincipalDeGoogle() {
        googleHomePage.ingresarAGoogle(prop.getProperty("url"));
    }

    @When("Busco una palabra o frase")
    public void buscoUnaPalabraOFrase() {
        world.palabraOFrase = new PalabraOFrase("Automatización");
        googleHomePage.buscarPalabraOFrase(world.palabraOFrase.getPalabraOFrase());
    }

    @Then("Se despliegan resultados relacionados")
    public void seDesplieganResultadosRelacionados() {
        googleSearchPage.verificarTituloPrimerResultado(world.palabraOFrase.getPalabraOFrase());
    }


}
