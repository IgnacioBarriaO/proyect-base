package corp.ripley.proyectobase.stepdefinitions;

import corp.ripley.proyectobase.utils.WebDriverFactory;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

public class Hooks {
    //Generalmetne se utiliza para preparar y destruir el ambiente (un navegador).

    public WebDriver driver;
    public Scenario scenario;
    public static String nombreTest;
    private WebDriverFactory webDriverFactory;

    @Before //Ocurre siempre ANTES de un ESCENARIO
    //Aqui vamos a ABRIR el navegador
    public void openBrowser(Scenario scenario){
        this.scenario = scenario; //Obtener el escenario que se ejecuta
        nombreTest = scenario.getName(); //Obtener el nombre del escenario
        System.out.println("Se va a ejecutar el escenario: " + nombreTest);
        webDriverFactory = new WebDriverFactory(); //Instancia del creador de Drivers
        driver = webDriverFactory.createWebDriver(nombreTest); //Instanciar el driver con el creador de Drivers y recibe como argumento el nombre del test
        driver.manage().deleteAllCookies(); //Borrar las cookies
        driver.manage().window().maximize(); //Maximizar la ventana
    }

    @After //Ocurre siempre DESPUES de un escenario
    //Aqui vamos a CERRAR el navegador
    public void tearDown(Scenario scenario){
        if(scenario.isFailed()) {
            try {
                scenario.log("Current Page URL is " + driver.getCurrentUrl());
                final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.attach(screenshot, "image/png", nombreTest);
            } catch (WebDriverException somePlatformsDontSupportScreenshots) {
                System.err.println(somePlatformsDontSupportScreenshots.getMessage());
            }
        }
        //Cierra el navegador y finaliza la sesión del WebDriver
        try {
            if(driver != null) {
                driver.manage().deleteAllCookies();
                driver.quit();
                //driver = null;
            }
        } catch (Exception e) {
            System.out.println("Fallo en ejecución: tearDown, Exception: " + e.getMessage());
        }
    }

}
