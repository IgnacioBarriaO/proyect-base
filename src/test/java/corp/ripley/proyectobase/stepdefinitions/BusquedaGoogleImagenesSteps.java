package corp.ripley.proyectobase.stepdefinitions;

import corp.ripley.proyectobase.models.PalabraOFrase;
import corp.ripley.proyectobase.models.Producto;
import corp.ripley.proyectobase.models.World;
import corp.ripley.proyectobase.pageobjects.GoogleImagePage;
import corp.ripley.proyectobase.pageobjects.GoogleSearchPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

public class BusquedaGoogleImagenesSteps {

    GoogleSearchPage googleSearchPage;
    GoogleImagePage googleImagePage;
    World world;
    Producto producto1;

    public BusquedaGoogleImagenesSteps(World world, GoogleSearchPage googleSearchPage, GoogleImagePage googleImagePage) {
        this.world = world;
        this.googleSearchPage = googleSearchPage;
        this.googleImagePage = googleImagePage;
    }


    @And("Ingreso a la seccion de imagenes")
    public void ingresoALaSeccionDeImagenes() {
        googleSearchPage.ingresarSeccionImagenes();
    }

    @Then("Se despliegan imagenes relacionadas con la busqueda")
    public void seDesplieganImagenesRelacionadasConLaBusqueda() {
        googleImagePage.verificarTituloPrimeraImagen(world.palabraOFrase.getPalabraOFrase());
    }
}
