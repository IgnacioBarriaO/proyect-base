package corp.ripley.proyectobase.pageobjects;

import corp.ripley.proyectobase.utils.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GoogleHomePage extends BasePage {
    //Se utilizará Selenium

    //Aca se colocan: Los elementos de la pagina y las acciones

    //Elementos Web
    @FindBy(name = "q")
    private WebElement barraBusqueda;

    @FindBy(name = "btnK")
    private WebElement btnBusqueda;

    //Acciones
    public void ingresarAGoogle(String url){
        log.debug("Navegación hacia la url: " + url);
        driver.get(url);
    }

    public void buscarPalabraOFrase(String palabraOFrase){
        log.debug("Escribiendo en la barra de busqueda: " + palabraOFrase);
        wait.until(ExpectedConditions.visibilityOf(barraBusqueda));
        barraBusqueda.sendKeys(palabraOFrase);
        log.debug("Se procede a hacer click en el botón Buscar con Google");
        //Esperar hasta que el elemento sea visible:
        wait.until(ExpectedConditions.visibilityOf(btnBusqueda));
        //Esperar hasta que el elemento sea cliqueable
        wait.until(ExpectedConditions.elementToBeClickable(btnBusqueda));
        btnBusqueda.click();
        log.info("Se hizo click en el botón Buscar con Google, se busca en Google: " + palabraOFrase);
    }
}
