package corp.ripley.proyectobase.pageobjects;

import corp.ripley.proyectobase.utils.BasePage;
import corp.ripley.proyectobase.utils.MetodosGenericos;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

public class GoogleImagePage extends BasePage {
    //Elementos
    @FindBy(xpath="//*[@id=\"islrg\"]/div[1]/div[1]/a[2]")
    private WebElement tituloImagenPrimerResultado;


    public void verificarTituloPrimeraImagen(String palabraOFraseBuscada){
        log.debug("Verificando el título del primer resultado");
        wait.until(ExpectedConditions.visibilityOf(tituloImagenPrimerResultado));
        String tituloPrimeraImagen = tituloImagenPrimerResultado.getText().toUpperCase();

        log.info("Título de la primera imagen: "+tituloPrimeraImagen);

        //Corroborar si el texto de la imagen del primer resutlado contiene la palabra o frase buscada
        Assert.assertTrue(tituloPrimeraImagen.contains(palabraOFraseBuscada.toUpperCase()));
        log.info("El título de la primera imagen contiene la palabra o frase buscada: " + palabraOFraseBuscada);
    }
}
