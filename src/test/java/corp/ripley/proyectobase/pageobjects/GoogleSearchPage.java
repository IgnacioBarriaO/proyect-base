package corp.ripley.proyectobase.pageobjects;

import corp.ripley.proyectobase.utils.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class GoogleSearchPage extends BasePage {
    //Elementos
    @FindBy(xpath = "//*[@id=\"rso\"]/div[1]/div/div/div/div[1]/a/h3")
    private WebElement tituloPrimerResultado;

    @FindBy(linkText = "Imágenes")
    private WebElement menuImagenes;

    //Accniones
    public void verificarTituloPrimerResultado(String palabraOFraseBuscada){
        log.debug("Verificando título del primer resultado");
        wait.until(ExpectedConditions.visibilityOf(tituloPrimerResultado));
        //Obtener el texto del primer resultado, y los convertimos a mayusculas
        String textoPrimerResultado = tituloPrimerResultado.getText().toUpperCase();
        log.info("El texto del primer resultado es: " + textoPrimerResultado);

        //Corroborar si el texto del primer resultado contiene la palabra o frase buscada
        Assert.assertTrue(textoPrimerResultado.contains(palabraOFraseBuscada.toUpperCase()));
        log.info("Primer resultado contiene la palabra " + palabraOFraseBuscada);

        /*
        Manera más larga
        String textoPrimerResultado = tituloPrimerResultado.getText();

        textoPrimerResultado = textoPrimerResultado.toUpperCase(); //Transforma el String A MAYUSCULA
        palabraOFraseBuscada = palabraOFraseBuscada.toUpperCase(); //Transfroma el String a MAYUSCULA
        System.out.println("El texto del primer resultado es: " + textoPrimerResultado);

        //Si el textoPrimerResultado CONTIENE la palabraOFraseBuscada, DEVUELVE VERDADERO
        boolean titutloContieneLaFrase = textoPrimerResultado.contains(palabraOFraseBuscada);
        Assert.assertTrue(titutloContieneLaFrase);
        System.out.println("Primer resultado contiene la palabra " + palabraOFraseBuscada);
        */
    }

    public void ingresarSeccionImagenes(){
        log.debug("Haciendo click en el menú de imágenes");
        wait.until(ExpectedConditions.elementToBeClickable(menuImagenes));
        menuImagenes.click();
        log.info("Ingresando a la sección de Imágenes");
    }
}
