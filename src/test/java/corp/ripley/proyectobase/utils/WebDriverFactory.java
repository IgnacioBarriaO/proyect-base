package corp.ripley.proyectobase.utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Clase Utilitaria que setea los drivers a utilizar para el Browser deseado según el navegador definido
 * en la propiedad de sistema "browser".
 * Customiza el driver a través de un switch y permite seleccionar propiedades
 * para cada navegador.
 * Por defecto el browser que se abrirá será chrome.
 */
public class WebDriverFactory {
    public static ThreadLocal<WebDriver> tlDriver = new ThreadLocal<>();

    /**
     * Crea un driver específico al navegador definido en la propiedad de sistema "browser".
     * Posteriormente devuelve el driver específico al Thread desde donde se llama.
     * @param nombreTest (Opcional) Nombre del Test o Escenario para determinar el nombre en Zalenium y Docker
     * @return Devuelve un WebDriver específico para un Thread.
     */
    public WebDriver createWebDriver(String ...nombreTest) {
        String testName = nombreTest.length > 0 ? nombreTest[0] : "";
        //Se obtiene la propiedad browser y se define como por defecto "chrome" si no esta definida
        String webdriver = System.getProperty("browser", "chrome");

        switch(webdriver) {

            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                FirefoxOptions fo = new FirefoxOptions();

                ProfilesIni profileIni = new ProfilesIni();
                FirefoxProfile profile = profileIni.getProfile("default");
                profile.setAcceptUntrustedCertificates(true);
                profile.setPreference("browser.safebrowsing.enabled", false);
                profile.setPreference("extensions.blocklist.enabled", false);
                profile.setPreference("browser.tabs.warnOnClose", false);
                profile.setPreference("browser.tabs.warnOnOpen", false);
                profile.setPreference("browser.safebrowsing.malware.enabled", false);
                profile.setPreference("webdriver_accept_untrusted_certs", true);
                profile.setPreference("webdriver_enable_native_events", true);
                profile.setPreference("webdriver_assume_untrusted_issuer", true);

                fo.addArguments("-private");
                fo.setProfile(profile);

                tlDriver.set(new FirefoxDriver(fo));
                return getDriver();

            case "chrome":
                WebDriverManager.chromedriver().setup();
                ChromeOptions co = new ChromeOptions();
                co.addArguments("--disable-gpu");
                co.addArguments("--start-maximized");
                co.addArguments("--no-sandbox");
                co.addArguments("--ignore-certificate-errors");
                co.addArguments("--disable-popup-blocking");
                co.addArguments("--window-size=1920,1080");
                co.addArguments("--disable-dev-shm-usage");
                co.addArguments("--lang=es");

                tlDriver.set(new ChromeDriver(co));
                return getDriver();

            case "chromeDocker":
                WebDriverManager.chromedriver().setup();
                ChromeOptions cop = new ChromeOptions();
                cop.addArguments("--headless");
                cop.addArguments("--disable-gpu");
                cop.addArguments("--start-maximized");
                cop.addArguments("--no-sandbox");
                cop.addArguments("--ignore-certificate-errors");
                cop.addArguments("--disable-popup-blocking");
                cop.addArguments("--window-size=1920,1080");
                cop.addArguments("--disable-dev-shm-usage");
                cop.addArguments("--lang=es");
                cop.setCapability("name", testName);

                try {
                    tlDriver.set(new RemoteWebDriver(new URL("http://10.0.149.90:4444/wd/hub"), cop));
                    return getDriver();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

            case "chromeZalenium":
                WebDriverManager.chromedriver().setup();
                ChromeOptions copt = new ChromeOptions();
                copt.addArguments("--disable-gpu");
                copt.addArguments("--start-maximized");
                copt.addArguments("--no-sandbox");
                copt.addArguments("--ignore-certificate-errors");
                copt.addArguments("--disable-popup-blocking");
                copt.addArguments("--window-size=1920,1080");
                copt.addArguments("--disable-dev-shm-usage");
                copt.addArguments("--lang=es");
                // Con esto le inserto el nombre de la prueba al driver, para ver el nombre en Zalenium
                copt.setCapability("name", testName);

                try {
                    tlDriver.set(new RemoteWebDriver(new URL("http://10.0.149.90:4442/wd/hub"), copt));
                    return getDriver();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

            case "ie":
                WebDriverManager.iedriver().setup();
                InternetExplorerOptions ieo = new InternetExplorerOptions();
                tlDriver.set(new InternetExplorerDriver(ieo));
                return getDriver();

            case "safari":
                WebDriverManager.safaridriver().setup();
                tlDriver.set(new SafariDriver());
                return getDriver();

            case "edge":
                WebDriverManager.edgedriver().setup();
                tlDriver.set(new EdgeDriver());
                return getDriver();

            default:
                throw new RuntimeException("Webdriver no soportado: " + webdriver);
        }
    }

    /**
     * Método para obtener el driver creado con el método createWebDriver()
     * @return Retorna el driver específico según el Thread que creo el Driver.
     */
    public static  WebDriver getDriver() {
        return tlDriver.get();
    }
}

