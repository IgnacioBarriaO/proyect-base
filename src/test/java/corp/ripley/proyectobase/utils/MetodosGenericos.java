package corp.ripley.proyectobase.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class MetodosGenericos {

    public static Properties readPropertiesFile(String fileName) throws IOException {
        FileInputStream fis = null;
        Properties prop = null;
        try {
            fis = new FileInputStream(fileName);
            prop = new Properties();
            prop.load(fis);
        } catch(IOException fnfe) {
            fnfe.printStackTrace();
        } finally {
            if (fis != null) {
                fis.close();
            }
        }
        return prop;
    }

    /**
     * Método que permite determinar el archivo properties con datos segun el ambiente determinado en la propiedad de sistema 'env',
     * los archivos pueden ser 'dataqa.properties' o 'dataprod.properties'
     * @param rutaDeProperties Path o ruta donde se encuentran los archivo properties, si esta en la raiz del proyecto, no debe entregarse argumento.
     *                         La ruta tradicional es: 'src/test/resources/'
     */
    public static Properties determinarPropertiesPorAmbiente(String ...rutaDeProperties) throws IOException {
        String env = System.getProperty("env", "qa");
        if(rutaDeProperties.length == 0){
            rutaDeProperties = new String[]{""};
        }
        else{
            rutaDeProperties[0] = rutaDeProperties[0].trim();
            if (!rutaDeProperties[0].substring(rutaDeProperties[0].length() - 1).equals("/") && !rutaDeProperties[0].substring(rutaDeProperties[0].length() - 1).equals("\\")){
                rutaDeProperties[0] += "/";
            };
        }
        return switch (env) {
            case "qa" -> readPropertiesFile(rutaDeProperties[0] + "dataqa.properties");
            case "dev" -> readPropertiesFile(rutaDeProperties[0] + "datadev.properties");
            case "prod" -> readPropertiesFile(rutaDeProperties[0] + "dataprod.properties");
            default -> throw new IOException ("No existe el ambiente: '" + env +"'");
        };
    }

}
