package corp.ripley.proyectobase.models;

public class PalabraOFrase {
    //Atributos
    private String palabraOFrase;

    //Metodos (Acciones)
    public PalabraOFrase(String palabraOFrase){
        this.palabraOFrase = palabraOFrase;
    }

    //Metodos Getter --> PAra obtener el valor de un atributo
    public String getPalabraOFrase(){
        return this.palabraOFrase;
    }

    //Metoodos Setter --> Para modificar el valor de un atributo
    public void setPalabraOFrase(String palabraOFrase){
        this.palabraOFrase = palabraOFrase;
    }

}
